package ru.tcsbank.currency;

import java.util.Scanner;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class Runner {

    public static void main(String[] args) {
        System.out.println("Currency exchange rate...");
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter FROM currency");
        CurrencyType from = getCurrency(sc);
        System.out.println("Enter TO currency");
        CurrencyType to = getCurrency(sc);

        double rate = ExchangeService.getRate(from, to);
        System.out.println(from + " => " + to + " : " + rate);
        System.exit(1);
    }

    private static CurrencyType getCurrency(Scanner sc) {
        while (true) {
            CurrencyType currency = ExchangeService.validateCurrency(sc.next());
            if (currency != null) {
                return currency;
            }
        }
    }
}
