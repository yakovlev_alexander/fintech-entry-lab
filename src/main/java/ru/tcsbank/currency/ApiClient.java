package ru.tcsbank.currency;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.tcsbank.currency.json.RatesDeserializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class ApiClient {

    private static Gson gson = new GsonBuilder()
            .registerTypeAdapter(ApiResponse.RateObject.class, new RatesDeserializer())
            .create();

    public static ApiResponse getExchangeRate(CurrencyType fromCurr, CurrencyType toCurr) {
        try {
            String query = "http://api.fixer.io/latest?base=" + fromCurr + "&symbols=" + toCurr;
            URL url = new URL(query);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept-Charset", "UTF-8");

            try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                return gson.fromJson(in, ApiResponse.class);
            }
        } catch (IOException e) {
            System.out.println("Web server error. Unable to GET exchange rates.");
            System.exit(-1);
            return null;
        }
    }
}
