package ru.tcsbank.currency;

import java.util.Date;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class RateOnDate {
    private Date date;
    private double rate;

    public RateOnDate(Date date, double rate) {
        this.date = date;
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public double getRate() {
        return rate;
    }
}
