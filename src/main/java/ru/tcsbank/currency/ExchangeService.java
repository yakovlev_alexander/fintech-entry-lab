package ru.tcsbank.currency;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.joda.time.DateTimeComparator;

import java.io.*;
import java.util.Date;
import java.util.concurrent.*;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class ExchangeService {

    private static Gson gson = new GsonBuilder().create();

    private static ExecutorService executorService = Executors.newFixedThreadPool(2);

    public static CurrencyType validateCurrency(String currencyType) {
        try {
            return CurrencyType.valueOf(currencyType.trim().toUpperCase());
        } catch (IllegalArgumentException e) {
            System.out.println("No such currency! Try another:");
            return null;
        }
    }

    public static double getRate(CurrencyType from, CurrencyType to) {
        Future<Double> future = executorService.submit(() -> {
            Thread.sleep(3000);
            File cacheFile = getCacheFile(from, to);
            if (cacheFile.exists()) {
                try (BufferedReader reader = new BufferedReader(new FileReader(cacheFile))) {
                    RateOnDate rateOnDate = gson.fromJson(reader, RateOnDate.class);
                    if (DateTimeComparator.getDateOnlyInstance().compare(rateOnDate.getDate(), new Date()) == 0) {
                        return rateOnDate.getRate();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return cacheAndGetRate(from, to);
        });

        try {
            // Waiting
            while (!future.isDone()) {
                Thread.sleep(500);
                System.out.print(".");
            }
            System.out.println(".");

            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("500 Server error. Unable to get rate.");
            System.exit(-1);
            return 0.;
        }
    }

    private static double cacheAndGetRate(CurrencyType from, CurrencyType to) {
        ApiResponse apiResponse = ApiClient.getExchangeRate(from, to);
        double rate = apiResponse.getRate();
        cache(from, to, new RateOnDate(new Date(), rate));
        return rate;
    }

    private static void cache(CurrencyType from, CurrencyType to, RateOnDate rateOnDate) {
        File cacheFile = getCacheFile(from, to);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(cacheFile))) {
            String json = gson.toJson(rateOnDate);
            writer.write(json);
        } catch (IOException e) {
            System.out.println("Cache error. Unable to write in cache.");
            System.exit(-1);
        }
    }

    private static File getCacheFile(CurrencyType from, CurrencyType to) {
        File cacheFile = new File(from + "_" + to + ".cache");
        if (cacheFile.exists()) {
            try {
                cacheFile.createNewFile();
            } catch (IOException e) {
                System.out.println("Cache error. Unable to create cache file.");
                System.exit(-1);
            }
        }
        return cacheFile;
    }
}
