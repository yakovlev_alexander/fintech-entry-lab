package ru.tcsbank.currency.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import ru.tcsbank.currency.ApiResponse;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class RatesDeserializer implements JsonDeserializer<ApiResponse.RateObject> {

    @Override
    public ApiResponse.RateObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ApiResponse.RateObject rate = null;
        if (json.isJsonObject()) {
            Set<Map.Entry<String, JsonElement>> entries = json.getAsJsonObject().entrySet();
            if (entries.size() > 0) {
                Map.Entry<String, JsonElement> entry = entries.iterator().next();
                rate = new ApiResponse.RateObject(entry.getKey(), entry.getValue().getAsDouble());
            }
        }
        return rate;
    }
}
