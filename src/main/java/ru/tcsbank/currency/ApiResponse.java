package ru.tcsbank.currency;

/**
 * Created by Alexander Yakovlev on 09/02/2018.
 */
public class ApiResponse {
    private String base;
    private RateObject rates;

    public static class RateObject {
        private String name;
        private double rate;

        public RateObject(String name, double rate) {
            this.name = name;
            this.rate = rate;
        }
    }

    public double getRate() {
        return rates.rate;
    }
}
